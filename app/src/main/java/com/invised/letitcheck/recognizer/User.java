package com.invised.letitcheck.recognizer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable, Identified {

    private static int sIdCounter = 1;
    private int mId;

    public String mName;
    public int mColor;

    public List<RecognizedItem> mRecognizedItems = new ArrayList<>();

    public User(String name, int color) {
        mName = name;
        mColor = color;

        mId = sIdCounter++;
    }

    @Override
    public int getId() {
        return mId;
    }
}