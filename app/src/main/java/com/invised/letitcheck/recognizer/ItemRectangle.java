package com.invised.letitcheck.recognizer;

import android.graphics.RectF;

import com.invised.letitcheck.common.SRectF;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ItemRectangle implements Identified, Serializable {

    private static int sIdCounter = 1;
    private int mId;

    public SRectF mRect;
    private List<User> mUsers = new ArrayList<>();

    public enum Type {
        TEXT, PRICE
    }

    public Type mType;

    public ItemRectangle(RectF rect, Type type) {
        mRect = new SRectF(rect);
        mType = type;

        mId = sIdCounter++;
    }

    @Override
    public String toString() {
        return "ItemRectangle{" +
                "mId=" + mId +
                ", mUsers=" + mUsers +
                ", mType=" + mType +
                '}';
    }

    @Override
    public int getId() {
        return mId;
    }

    public boolean hasUser(User user) {
        return mUsers.contains(user);
    }

    public boolean hasUsers() {
        return mUsers.size() != 0;
    }

    public void addUser(User user) {
        mUsers.add(user);
    }

    public List<User> getUsers() {
        return mUsers;
    }

    public int getUsersCount() {
        return mUsers.size();
    }

    public void removeUser(User user) {
        mUsers.remove(user);
    }
}
