package com.invised.letitcheck.recognizer;

import java.io.Serializable;

public class RecognizedItem implements Serializable {

    public String mTitle;
    public String mPrice;

    public RecognizedItem(String title, String price) {
        mTitle = title;
        mPrice = price;
    }
}