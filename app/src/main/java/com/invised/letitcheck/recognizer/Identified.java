package com.invised.letitcheck.recognizer;

/**
 * Created by Invised on 15.12.2015.
 */
public interface Identified {

    int getId();
}
