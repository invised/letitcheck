package com.invised.letitcheck.recognizer;

public class HistoryItem {

    public enum Action {
        ADDED, REMOVED
    }

    public Action mAction;

    public ItemRectangle mItemRectangle;
    public User mUser;

    public HistoryItem(Action action, ItemRectangle itemRectangle, User user) {
        mAction = action;
        mItemRectangle = itemRectangle;
        mUser = user;
    }
}
