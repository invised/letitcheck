package com.invised.letitcheck.common;

import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;

import com.invised.letitcheck.fragment.dialogs.ProgressFragmentDialog;

public abstract class ProgressAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    public abstract String getMessage();

    private ProgressFragmentDialog mProgressFragmentDialog;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mProgressFragmentDialog = ProgressFragmentDialog.newInstance(getMessage());
        mProgressFragmentDialog.show(mFragmentManager, null);
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);

        mProgressFragmentDialog.dismissAllowingStateLoss();
    }

    private FragmentManager mFragmentManager;

    public ProgressAsyncTask(FragmentManager fragmentManager) {
        mFragmentManager = fragmentManager;
    }
}