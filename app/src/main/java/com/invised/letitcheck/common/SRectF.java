package com.invised.letitcheck.common;

import android.graphics.Rect;
import android.graphics.RectF;

import java.io.Serializable;

public class SRectF extends RectF implements Serializable {

    public SRectF(Rect r) {
        super(r);
    }

    public SRectF(RectF r) {
        super(r);
    }
}
