package com.invised.letitcheck.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import edu.kpi.receipt_recognizer.pojo.Rectangle;

public class Utils {

    public static String makeAssetImageUri(String imageNameNoExt) {

        return "file:///android_asset/photos/" + imageNameNoExt + ".jpg";
    }

    public static <T> T obtainListener(Object... objects) {

        T listener;
        for (Object obj : objects) {
            if (obj != null) {
                try {
                    listener = (T) obj;
                    return listener;
                } catch (ClassCastException e) {
                    // Skip.
                }
            }
        }

        throw new IllegalArgumentException("Cannot find listener.");
    }

    public static void closeCursor(Cursor cursor) {

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    public static void removeGlobalLayoutListener(View view, ViewTreeObserver.OnGlobalLayoutListener listener) {

        if (Build.VERSION.SDK_INT < 16) {
            view.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
        } else {
            view.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
        }
    }

    public static String concatenate(List<?> list, String delimiter) {

        StringBuilder builder = new StringBuilder();
        int i = 0;
        for (Object obj : list) {
            builder.append(obj);
            if (i != list.size() - 1) {
                builder.append(delimiter);
            }
            i++;
        }

        return builder.toString();
    }

    public static <T extends View> T findView(View view, @IdRes int id) {
        return (T) view.findViewById(id);
    }

    public static <T extends View> T findView(Activity activity, @IdRes int id) {
        return (T) activity.findViewById(id);
    }

    public static <T extends View> T findView(View view, Class<T> clazz, @IdRes int id) {
        return (T) view.findViewById(id);
    }

    public static  <T extends Fragment> T findFragment(FragmentManager manager, @IdRes int id) {
        return (T) manager.findFragmentById(id);
    }

    public static  <T extends Fragment> T findFragment(FragmentManager manager, String tag) {
        return (T) manager.findFragmentByTag(tag);
    }

    public static  <T extends android.app.Fragment> T findFragment(android.app.FragmentManager manager, @IdRes int id) {
        return (T) manager.findFragmentById(id);
    }

    public static  <T extends android.app.Fragment> T findFragment(android.app.FragmentManager manager, String tag) {
        return (T) manager.findFragmentByTag(tag);
    }

    public static int toVisibility(boolean visible) {
        return visible ? View.VISIBLE : View.GONE;
    }

    public static String getString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndexOrThrow(columnName));
    }

    public static int getInt(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(columnName));
    }

    public static long getLong(Cursor cursor, String columnName) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(columnName));
    }

    public static double getDouble(Cursor cursor, String columnName) {
        return cursor.getDouble(cursor.getColumnIndexOrThrow(columnName));
    }

    public static boolean isPackageInstalled(String packageName, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void openAppInPlayMarket(String appPackageName, Context context) {

        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static void checkExtras(Intent intent, String... keys) {

        for (String key : keys) {
            if (!intent.hasExtra(key)) {
                throw new IllegalArgumentException(key + " is not specified.");
            }
        }
    }

    public static void checkExtras(Bundle bundle, String... keys) {

        if (bundle == null && keys.length != 0) {
            throw new IllegalArgumentException("Bundle should not be null.");
        }

        for (String key : keys) {
            if (!bundle.containsKey(key)) {
                throw new IllegalArgumentException(key + " is not specified.");
            }
        }
    }

    public static void checkIntegerExtras(Intent intent, String... keys) {

        for (String key : keys) {
            if (intent.getIntExtra(key, -1) == -1) {
                throw new IllegalArgumentException(key + " is not specified.");
            }
        }
    }

    public static <T extends View> T inflate(Context context, @LayoutRes int id) {
        return (T) LayoutInflater.from(context).inflate(id, null);
    }

    public static <T extends View> T inflate(Context context, @LayoutRes int id, ViewGroup parent) {
        return (T) LayoutInflater.from(context).inflate(id, parent, false);
    }

    public static String toFileUri(String path) {

        return "file:///" + path;
    }

    public static void copyAssets(String assetPath, File dstFolder, AssetManager assetManager) throws IOException {

        final String[] assetFiles = assetManager.list(assetPath);

        for (String relativeAssetPath : assetFiles) {
            InputStream assetStream = null;
            OutputStream fileStream = null;
            try {
                assetStream = assetManager.open(assetPath + File.separator + relativeAssetPath);
                fileStream = new FileOutputStream(dstFolder.getPath() + File.separator + relativeAssetPath);

                byte[] buffer = new byte[1024];
                int read;
                while ((read = assetStream.read(buffer)) != -1) {
                    fileStream.write(buffer, 0, read);
                }
            } finally {
                close(fileStream);
                close(assetStream);
            }
        }
    }

    private static void close(Closeable closeable) throws IOException {

        if (closeable != null) {
            closeable.close();
        }
    }

    public static File generateJpegFileName(File storageDir) {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = storageDir + File.separator + "JPEG_" + timeStamp + "_" + ".jpg";

        return new File(imageFileName);
    }

    public static File getPublicPhotosDir() {

        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);

        return storageDir;
    }

    public static void scanMediaFiles(Context context, String... filePaths) {

        MediaScannerConnection.scanFile(context, filePaths, null, null);
    }

    public static Rect toRect(RectF rectF) {
        return new Rect((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom);
    }

    public static RectF toRectF(Rectangle rectangle) {

        return new RectF(toRect(rectangle));
    }

    public static Rect toRect(Rectangle rectangle) {

        final int left = rectangle.getX();
        final int top = rectangle.getY();

        final int right = rectangle.getMaxX();
        final int bottom = rectangle.getMaxY();

        return new Rect(left, top, right, bottom);
    }

    public static Bitmap createRotatedBitmap(int rotateDegrees, String imagePath) {

        Bitmap myImg = BitmapFactory.decodeFile(imagePath);

        Matrix matrix = new Matrix();
        matrix.postRotate(rotateDegrees);

        Bitmap rotated = Bitmap.createBitmap(myImg, 0, 0, myImg.getWidth(), myImg.getHeight(),
                matrix, true);

        return rotated;
    }

    public static void writeBitmap(String filename, Bitmap bitmap) {

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
