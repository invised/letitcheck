package com.invised.letitcheck.util;

import com.invised.letitcheck.BuildConfig;

public class AppConfig {

    public static boolean DEBUG =
            BuildConfig.DEBUG
//            true
//            false
            ;

    public static final boolean RELEASE = !DEBUG;
    public static final boolean ENABLE_CRASHLYTICS = false || RELEASE;
}
