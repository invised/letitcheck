package com.invised.letitcheck.fragment.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.invised.letitcheck.R;
import com.invised.letitcheck.util.Utils;


public class ImageSourceChooserDialog extends BasicDialogFragment {

    private OnPhotoSourceSelectedListener mListener;

    public interface OnPhotoSourceSelectedListener {

        void onPhotoSourceSelected(PhotoSource source);
    }

    public enum PhotoSource {
        CAMERA, GALLERY
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mListener = Utils.obtainListener(getTargetFragment(), getActivity());
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        CharSequence[] items = {getString(R.string.photo_source_camera), getString(R.string.photo_source_gallery)};

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.photos_source_title)
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        mListener.onPhotoSourceSelected(which == 0 ? PhotoSource.CAMERA : PhotoSource.GALLERY);
                    }
                })
                .create();
    }
}