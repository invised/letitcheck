package com.invised.letitcheck.fragment.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.invised.letitcheck.R;

public class IntroDialog extends BasicDialogFragment {

    public static final String KEY_INTRO_SHOWED = "introShowed";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setView(R.layout.fragment_intro)
                .setPositiveButton(com.invised.letitcheck.R.string.intro_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putBoolean(KEY_INTRO_SHOWED, true).apply();
                    }
                })
                .create();
    }
}
