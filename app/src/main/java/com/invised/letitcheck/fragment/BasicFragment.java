package com.invised.letitcheck.fragment;

import android.support.v4.app.Fragment;

import com.invised.letitcheck.activity.BasicActivity;
import com.invised.letitcheck.util.Utils;

/**
 * Created by Invised on 09.09.2015.
 */
public class BasicFragment extends Fragment {

    public BasicActivity getBasicActivity() {
        return (BasicActivity) getActivity();
    }

    public void checkExtras(String... keys) {
        Utils.checkExtras(getArguments(), keys);
    }
}
