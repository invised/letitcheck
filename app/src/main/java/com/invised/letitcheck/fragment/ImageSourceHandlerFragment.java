package com.invised.letitcheck.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.invised.letitcheck.util.AppConfig;
import com.invised.letitcheck.R;
import com.invised.letitcheck.fragment.dialogs.ImageSourceChooserDialog;
import com.invised.letitcheck.util.Utils;

import java.io.File;
import java.io.IOException;

public class ImageSourceHandlerFragment extends BasicFragment
    implements ImageSourceChooserDialog.OnPhotoSourceSelectedListener {

    private static final int ACTIVITY_CAMERA = 1;
    private static final int ACTIVITY_GALLERY = 2;
    public static final String EXTRA_FILE = "file";

    private OnPhotoReceivedListener mListener;
    private File mFileToSave;

    public void showChooserDialog(File fileToSave) {

        mFileToSave = fileToSave;

        ImageSourceChooserDialog chooserDialog = new ImageSourceChooserDialog();
        chooserDialog.setTargetFragment(this, -1);
        chooserDialog.show(getFragmentManager(), null);
    }

    public void setFileToSave(File fileToSave) {
        mFileToSave = fileToSave;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mFileToSave != null) {
            outState.putString(EXTRA_FILE, mFileToSave.getAbsolutePath());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null && savedInstanceState.getString(EXTRA_FILE) != null) {
            mFileToSave = new File(savedInstanceState.getString(EXTRA_FILE));
        }
    }

    @Override
    public void onPhotoSourceSelected(ImageSourceChooserDialog.PhotoSource source) {

        try {
            switch (source) {
                case CAMERA:
                    dispatchTakePictureIntent();
                    break;

                case GALLERY:
                    dispatchGalleryIntent();
                    break;
            }
        } catch (IOException e) {
            Toast.makeText(getActivity(), R.string.error_temp_file, Toast.LENGTH_SHORT).show();
            if (AppConfig.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public void dispatchGalleryIntent() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        if (canHandle(photoPickerIntent)) {
            startActivityForResult(photoPickerIntent, ACTIVITY_GALLERY);
        } else {
            Toast.makeText(getActivity(), R.string.error_launch, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean canHandle(Intent photoPickerIntent) {
        return photoPickerIntent.resolveActivity(getActivity().getPackageManager()) != null;
    }

    public void dispatchTakePictureIntent() throws IOException {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Ensure that there's a camera activity to handle the intent
        if (canHandle(takePictureIntent)) {

            // Create the File where the photo should go
            mFileToSave.createNewFile();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(mFileToSave));
            startActivityForResult(takePictureIntent, ACTIVITY_CAMERA);
        } else {
            Toast.makeText(getActivity(), R.string.error_launch, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mListener = Utils.obtainListener(getTargetFragment(), getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case ACTIVITY_GALLERY: {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(
                        selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();

                mListener.onPhotoReceived(filePath, ImageSourceChooserDialog.PhotoSource.GALLERY);
            }
            break;
            case ACTIVITY_CAMERA: {
                mListener.onPhotoReceived(mFileToSave.getAbsolutePath(), ImageSourceChooserDialog.PhotoSource.CAMERA);
            }
            break;
        }
    }

    public static ImageSourceHandlerFragment commit(FragmentManager manager, Fragment targetFragment) {

        final String TAG = "PhotoSourceHandler";

        ImageSourceHandlerFragment fragment = Utils.findFragment(manager, TAG);
        if (fragment != null) {
            return fragment;
        }

        fragment = new ImageSourceHandlerFragment();

        if (targetFragment != null) {
            fragment.setTargetFragment(targetFragment, -1);
        }
        manager.beginTransaction().add(fragment, TAG).commit();

        return fragment;
    }

    public interface OnPhotoReceivedListener {

        void onPhotoReceived(String filePath, ImageSourceChooserDialog.PhotoSource source);
    }
}
