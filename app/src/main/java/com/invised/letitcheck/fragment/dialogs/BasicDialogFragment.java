package com.invised.letitcheck.fragment.dialogs;

import android.support.v4.app.DialogFragment;

import com.invised.letitcheck.activity.BasicActivity;
import com.invised.letitcheck.util.Utils;

public class BasicDialogFragment extends DialogFragment {

    public BasicActivity getBasicActivity() {
        return (BasicActivity) getActivity();
    }

    public void checkExtras(String... keys) {
        Utils.checkExtras(getArguments(), keys);
    }
}
