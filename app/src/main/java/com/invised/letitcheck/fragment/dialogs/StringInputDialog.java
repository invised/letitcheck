package com.invised.letitcheck.fragment.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.invised.letitcheck.R;
import com.invised.letitcheck.util.Utils;

import static com.invised.letitcheck.util.Utils.findView;
import static com.invised.letitcheck.util.Utils.inflate;

public class StringInputDialog extends BasicDialogFragment
		implements View.OnClickListener, OnShowListener, TextWatcher {

	public static final String KEY_TEXT = "preset_name";

	public static final String ARG_HINT = "hint";
	public static final String ARG_BUTTON_TEXT = "buttonText";
	public static final String ARG_TAG = "tag";

	private AlertDialog mDialog;
	private Button mPositiveButton;

	private EditText mEditText;
	private StringDialogListener mListener;

	private boolean mCommited;

	public interface StringDialogListener {

		void onStringCommitted(String text, String tag);

		void onStringInputCancelled(String tag);
	}

	public static StringInputDialog newInstance(String hint, String positiveText, String tag, Fragment targetFragment) {

		Bundle bundle = new Bundle();
		bundle.putString(ARG_HINT, hint);
		bundle.putString(ARG_BUTTON_TEXT, positiveText);
		bundle.putString(ARG_TAG, tag);

		StringInputDialog dialog = new StringInputDialog();
        dialog.setTargetFragment(targetFragment, -1);
		dialog.setArguments(bundle);

		return dialog;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

        mListener = Utils.obtainListener(getTargetFragment(), activity);
	}

	@Override
	public void afterTextChanged(Editable s) {

		syncButtonState(s.toString());
	}

	private void syncButtonState(String s) {

		if (mPositiveButton != null) {
			mPositiveButton.setEnabled(s.length() > 0);
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		View layout = inflate(getActivity(), R.layout.dialog_string_input);
		mEditText = findView(layout, R.id.editText);
		mEditText.setHint(getArguments().getString(ARG_HINT));

		if (savedInstanceState != null) {
			mEditText.setText(savedInstanceState.getString(KEY_TEXT));
		}

		mEditText.addTextChangedListener(this);

		mCommited = false;

		mDialog = new Builder(getActivity())
				.setView(layout)
				.setPositiveButton(getArguments().getString(ARG_BUTTON_TEXT), null)
				.create();

		mDialog.setOnShowListener(this);

		return mDialog;
	}

	@Override
	public void onClick(View v) {

		mCommited = true;

		String text = mEditText.getText().toString();
		mListener.onStringCommitted(text, getArguments().getString(ARG_TAG));

		dismiss();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		if (!mCommited && !getActivity().isChangingConfigurations()) {
			mListener.onStringInputCancelled(getArguments().getString(ARG_TAG));
		}
	}

	@Override
	public void onShow(DialogInterface dialog) {

		mPositiveButton = mDialog.getButton(AlertDialog.BUTTON_POSITIVE);
		mPositiveButton.setOnClickListener(this);

		syncButtonState(mEditText.getText().toString());
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putString(KEY_TEXT, mEditText.getText().toString());
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}
}