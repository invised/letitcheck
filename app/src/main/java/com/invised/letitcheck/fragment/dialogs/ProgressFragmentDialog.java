package com.invised.letitcheck.fragment.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;

public class ProgressFragmentDialog extends BasicDialogFragment {

    public static final String KEY_MESSAGE = "message";

    public static ProgressFragmentDialog newInstance(String message) {

        Bundle bundle = new Bundle();
        bundle.putString(KEY_MESSAGE, message);

        ProgressFragmentDialog fragment = new ProgressFragmentDialog();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setCancelable(false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final ProgressDialog dialog = new ProgressDialog(getActivity());

        final String message = getArguments().getString(KEY_MESSAGE);
        dialog.setMessage(message);

        return dialog;
    }
}