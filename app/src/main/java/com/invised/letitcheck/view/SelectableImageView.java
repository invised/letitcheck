package com.invised.letitcheck.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.invised.letitcheck.recognizer.ItemRectangle;
import com.invised.letitcheck.recognizer.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.invised.letitcheck.recognizer.ItemRectangle.Type.TEXT;


public class SelectableImageView extends SubsamplingScaleImageView {

    public static final String KEY_SUPER_STATE = "superState";
    public static final String KEY_ITEMS = "items";
    private RectF mImageRect;

    public SelectableImageView(Context context, AttributeSet attr) {
        super(context, attr);
    }

    public SelectableImageView(Context context) {
        super(context);
    }

    private List<ItemRectangle> mItemRectangles = new ArrayList<>();

    public void addRect(RectF rect, ItemRectangle.Type type) {

        mItemRectangles.add(new ItemRectangle(rect, type));
    }

    public void removeItem(ItemRectangle itemRectangle) {

        mItemRectangles.remove(itemRectangle);
    }

    public void setItemRectangles(List<ItemRectangle> itemRectangles) {
        mItemRectangles = itemRectangles;
    }

    private RectF mDrawRect = new RectF();

    private RectF sourceToViewRect(RectF sourceRect) {

        final PointF viewPoint1 = sourceToViewCoord(sourceRect.left, sourceRect.top);
        if (viewPoint1 != null) {
            mDrawRect.set(sourceRect);
            mDrawRect.left = viewPoint1.x;
            mDrawRect.top = viewPoint1.y;

            final PointF viewPoint2 = sourceToViewCoord(sourceRect.right, sourceRect.bottom);
            if (viewPoint2 != null) {
                mDrawRect.right = viewPoint2.x;
                mDrawRect.bottom = viewPoint2.y;
            }

        }

        // TODO: fills mDrawRect
        return mDrawRect;
    }

    public boolean getPointBelongsToImage(PointF viewPoint) {

        PointF sourcePoint = viewToSourceCoord(viewPoint);
        return mImageRect != null && mImageRect.contains(sourcePoint.x, sourcePoint.y);
    }

    @Override
    protected void onImageLoaded() {
        super.onImageLoaded();

        mImageRect = new RectF(0, 0, getSWidth(), getSHeight());
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();

        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_SUPER_STATE, superState);
        bundle.putSerializable(KEY_ITEMS, (Serializable) mItemRectangles);;

        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {

        Bundle bundle = (Bundle) state;
        super.onRestoreInstanceState(bundle.getParcelable(KEY_SUPER_STATE));

        mItemRectangles = (List<ItemRectangle>) bundle.getSerializable(KEY_ITEMS);
    }

    public ItemRectangle getClickedItem(float vx, float vy) {

        for (ItemRectangle itemRect : mItemRectangles) {
            PointF sPoint = viewToSourceCoord(vx, vy);
            if (itemRect.mRect.contains(sPoint.x, sPoint.y)) {
                return itemRect;
            }
        }

        return null;
    }

    public List<ItemRectangle> getUserItems(User user) {

        List<ItemRectangle> userRects = new ArrayList<>();
        for (ItemRectangle itemRect : mItemRectangles) {
            if (itemRect.hasUser(user)) {
                userRects.add(itemRect);
            }
        }

        return userRects;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int fillColor;
        int strokeColor;
        int alpha;
        for (ItemRectangle rectItem : mItemRectangles) {

            final boolean typeText = rectItem.mType == TEXT;
            final boolean noUser = !rectItem.hasUsers();

            final RectF viewRect = sourceToViewRect(rectItem.mRect);
            if (noUser) {
                // If not selected, make it really transparent
                alpha = 70;
                fillColor = Color.GRAY;

                drawItem(canvas, alpha, fillColor, viewRect);
                drawItemStroke(canvas, alpha, Color.BLACK, viewRect);

            } else {
                alpha = 150;

                final float widthPerUser = viewRect.width() / rectItem.getUsersCount();

                int i = 0;
                for (User user : rectItem.getUsers()) {

                    // TODO: remove allocation
                    RectF rect = new RectF(viewRect);
                    rect.left += widthPerUser * i;
                    rect.right = rect.left + widthPerUser;

                    fillColor = user.mColor;
                    drawItem(canvas, alpha, fillColor, rect);

                    i++;
                }

                drawItemStroke(canvas, alpha, Color.BLACK, viewRect);
            }
        }

    }

    private void drawItem(Canvas canvas, int alpha, int fillColor, RectF drawRect) {

        mFillPaint.setColor(fillColor);
        mFillPaint.setAlpha(alpha);
        canvas.drawRect(drawRect, mFillPaint);
    }

    private void drawItemStroke(Canvas canvas, int alpha, int strokeColor, RectF drawRect) {

        mStrokePaint.setColor(strokeColor);
        mStrokePaint.setAlpha(alpha);
        canvas.drawRect(drawRect, mStrokePaint);
    }

    private Paint mFillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    {
        mFillPaint.setStyle(Paint.Style.FILL);
    }

    private Paint mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    {
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mStrokePaint.setStrokeWidth(3);
    }

    private DashPathEffect mPathEffect = new DashPathEffect(PATH_INTERVALS, 0);
    public static final float[] PATH_INTERVALS = new float[]{5, 5};

}