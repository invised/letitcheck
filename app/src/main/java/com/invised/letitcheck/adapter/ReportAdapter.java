package com.invised.letitcheck.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.invised.letitcheck.R;
import com.invised.letitcheck.recognizer.RecognizedItem;
import com.invised.letitcheck.util.Utils;
import com.invised.letitcheck.recognizer.User;

import java.util.ArrayList;
import java.util.List;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ReportHolder> {

    public static final int TYPE_USER = 0;
    public static final int TYPE_VALUE = 1;
    public static final int TYPE_VALUE_TOTAL = 2;

    private List<TypeHolder> mTypesMap = new ArrayList<>();

    private List<User> mUsers;

    public ReportAdapter(List<User> users) {

        mUsers = users;

        buildTypesMap();

    }

    private void buildTypesMap() {

        mTypesMap.clear();

        for (User user : mUsers) {
            mTypesMap.add(new TypeHolder(user, TYPE_USER));
            for (int i = 0; i < user.mRecognizedItems.size(); i++) {
                mTypesMap.add(new TypeHolder(user, i, TYPE_VALUE));
            }
            mTypesMap.add(new TypeHolder(user, TYPE_VALUE_TOTAL));
        }
    }

    private class TypeHolder {

        User mUser;
        int mValueNum;
        int mType;

        public TypeHolder(int type) {
            mType = type;
        }

        public TypeHolder(User user, int type) {
            mUser = user;
            mType = type;
        }

        public TypeHolder(User user, int valueNum, int type) {
            mUser = user;
            mValueNum = valueNum;
            mType = type;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mTypesMap.get(position).mType;
    }

    @Override
    public ReportHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int layoutType = 0;

        switch (viewType) {

            case TYPE_USER:
                layoutType = R.layout.item_user;
                break;

            case TYPE_VALUE:
                layoutType = R.layout.item_value;
                break;

            case TYPE_VALUE_TOTAL:
                layoutType = R.layout.item_value_total;
                break;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(layoutType, parent, false);
        return new ReportHolder(view);
    }

    @Override
    public int getItemCount() {
        return mTypesMap.size();
    }

    @Override
    public void onBindViewHolder(ReportHolder holder, int position) {

        final TypeHolder typeHolder = mTypesMap.get(position);
        switch (getItemViewType(position)) {

            case TYPE_USER:
                holder.mText1.setText(typeHolder.mUser.mName);

                final int userColor = typeHolder.mUser.mColor;
                holder.mText1.setTextColor(userColor);
                break;

            case TYPE_VALUE:
                holder.mText1.setText(typeHolder.mUser.mRecognizedItems.get(typeHolder.mValueNum).mTitle);
                holder.mText2.setText(typeHolder.mUser.mRecognizedItems.get(typeHolder.mValueNum).mPrice);
                break;

            case TYPE_VALUE_TOTAL:

                final TotalValueHolder totalHolder = getTotal(typeHolder.mUser);
                float total = totalHolder.mTotalPrice;

                if (totalHolder.mHasErrors) {
                    holder.mText1.setTextColor(Color.RED);
                }

                holder.mText1.setText("Всього: " + String.format("%.2f", total));
                break;
        }

    }

    private TotalValueHolder getTotal(User user) {

        TotalValueHolder totalHolder = new TotalValueHolder();

        for (RecognizedItem recognizedItem : user.mRecognizedItems) {

            try {
                if (recognizedItem.mPrice != null) {
                    recognizedItem.mPrice = recognizedItem.mPrice.replace(',', '.');
                }
                Float price = Float.valueOf(recognizedItem.mPrice);
                totalHolder.mTotalPrice += price;
            } catch (NumberFormatException e) {
                totalHolder.mHasErrors = true;
            }
        }

        return totalHolder;
    }

    private static class TotalValueHolder {

        float mTotalPrice;
        boolean mHasErrors;
    }

    public class ReportHolder extends RecyclerView.ViewHolder {

        TextView mText1;
        TextView mText2;

        public ReportHolder(View itemView) {
            super(itemView);

            mText1 = Utils.findView(itemView, R.id.text1);
            mText2 = Utils.findView(itemView, R.id.text2);
        }
    }
}