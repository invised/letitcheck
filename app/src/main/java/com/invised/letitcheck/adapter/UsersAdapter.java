package com.invised.letitcheck.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.invised.letitcheck.R;
import com.invised.letitcheck.recognizer.User;
import com.invised.letitcheck.util.Utils;

import java.util.ArrayList;

import static com.invised.letitcheck.util.Utils.inflate;

public class UsersAdapter extends BaseAdapter implements SpinnerAdapter {

    private ArrayList<User> mUsers = new ArrayList<>();
    private Context mContext;

    public UsersAdapter(ArrayList<User> users, Context context) {
        mUsers = users;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mUsers.size();
    }

    @Override
    public User getItem(int position) {
        return mUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View layout = inflate(mContext, R.layout.item_spinner, parent);

        TextView textView = Utils.findView(layout, android.R.id.text1);
        final User item = getItem(position);
        textView.setText(item.mName);

        View colorMarker = Utils.findView(layout, R.id.color_marker);
        colorMarker.setBackgroundColor(item.mColor);

        return layout;
    }
}
