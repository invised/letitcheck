package com.invised.letitcheck;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.invised.letitcheck.util.AppConfig;

import org.opencv.android.OpenCVLoader;

import io.fabric.sdk.android.Fabric;

public class LetItApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (AppConfig.ENABLE_CRASHLYTICS) {
            Fabric.with(this, new Crashlytics());
        }

        if (!OpenCVLoader.initDebug()) {
            throw new RuntimeException("Open CV failed to initialize");
        }
    }

}
