package com.invised.letitcheck.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.googlecode.tesseract.android.TessBaseAPI;
import com.invised.letitcheck.R;
import com.invised.letitcheck.adapter.UsersAdapter;
import com.invised.letitcheck.common.ProgressAsyncTask;
import com.invised.letitcheck.fragment.BasicFragment;
import com.invised.letitcheck.fragment.dialogs.BasicDialogFragment;
import com.invised.letitcheck.fragment.dialogs.IntroDialog;
import com.invised.letitcheck.recognizer.HistoryItem;
import com.invised.letitcheck.recognizer.Identified;
import com.invised.letitcheck.recognizer.ItemRectangle;
import com.invised.letitcheck.recognizer.RecognizedItem;
import com.invised.letitcheck.recognizer.User;
import com.invised.letitcheck.util.AppConfig;
import com.invised.letitcheck.util.Utils;
import com.invised.letitcheck.view.SelectableImageView;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import edu.kpi.receipt_recognizer.core.TextRecognizer;
import edu.kpi.receipt_recognizer.parser.ReceiptParser;
import edu.kpi.receipt_recognizer.parser.ReceiptParserFactory;
import edu.kpi.receipt_recognizer.pojo.Receipt;
import edu.kpi.receipt_recognizer.pojo.Rectangle;
import edu.kpi.receipt_recognizer.pojo.TextRectangle;

import static com.invised.letitcheck.recognizer.ItemRectangle.Type.PRICE;
import static com.invised.letitcheck.recognizer.ItemRectangle.Type.TEXT;
import static com.invised.letitcheck.util.Utils.toFileUri;
import static com.invised.letitcheck.util.Utils.toRect;
import static com.invised.letitcheck.util.Utils.toRectF;


public class PickerActivity extends BasicActivity {

    private static final String KEY_IMAGE_PATH = "image_path";

    private static final String DATA_PATH =
            Environment.getExternalStorageDirectory().toString() + "/";

    private static final int[] MATERIAL_PALETTE = {0xFFF44336, 0xFF9C27B0, 0xFF3F51B5, 0xFF2196F3,
            0xFF009688, 0xFFCDDC39, 0xFFFFC107, 0xFFFF5722};

    private static final String MAIN_LANGUAGE = "ukr";
    private static final String KEY_USERS_MAP = "usersMap";
    private static final String KEY_USERS = "users";
    private static final String TAG_HOLDER_FRAGMENT = "holderFragment";

    private SelectableImageView mImageView;
    private Spinner mUsersSpinner;
    private UsersAdapter mAdapter;
    private String mImagePath;

    private String[] mAnimals;

    private List<HistoryItem> mHistory = new ArrayList<>();

    private TessBaseAPI mBaseApi;

    private Map<User, List<ItemRectangle>> mUserRectanglesMap = new HashMap<>();

    private TextRecognizer mTextRecognizer = new TextRecognizer() {

        @Override
        public String getText(Rectangle rectangle) throws Exception {

            return recognizeRect(toRect(rectangle));
        }
    };
    private ArrayList<User> mUsers = new ArrayList<>();
    private GestureDetector.SimpleOnGestureListener mGestureListener =
            new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    super.onSingleTapUp(e);

                    if (!canProcess(e)) {
                        return false;
                    }

                    if (mUsers.size() == 0) {
                        return true;
                    }

                    final ItemRectangle item = mImageView.getClickedItem(e.getX(), e.getY());

                    if (item != null) {
                        onItemClicked(item);
                    }

                    return true;
                }

                private boolean canProcess(MotionEvent e) {
                    return mImageView.isReady() && mImageView.getPointBelongsToImage(new PointF(e.getX(), e.getY()));
                }
            };

    public static void startActivity(Context context, String imagePath) {

        Intent intent = new Intent(context, PickerActivity.class);
        intent.putExtra(KEY_IMAGE_PATH, imagePath);
        context.startActivity(intent);
    }

    private void onItemClicked(ItemRectangle item) {

        final User currentUser = getCurrentUser();

        if (mUserRectanglesMap.get(currentUser) == null) {
            mUserRectanglesMap.put(currentUser, new ArrayList<ItemRectangle>());
        }

        HistoryItem.Action action;
        if (item.hasUsers()) {

            if (item.hasUser(currentUser)) {
                // We own that item

                action = HistoryItem.Action.REMOVED;
                removeUser(item, currentUser);
            } else {
                // There are other users, we don't own this item

                action = HistoryItem.Action.ADDED;
                OwnerConfirmationDialog.newInstance(item.getId(), currentUser.getId()).show(getSupportFragmentManager(), null);
            }

        } else {
            // Nobody owns this item.

            action = HistoryItem.Action.ADDED;
            addUser(item, currentUser);
        }

        HistoryItem historyItem = new HistoryItem(action, item, currentUser);
        mHistory.add(historyItem);

        mImageView.invalidate();
    }

    private void addUser(ItemRectangle item, User user) {

        item.addUser(user);
        mUserRectanglesMap.get(user).add(item);
    }

    private void removeUser(ItemRectangle item, User user) {

        item.removeUser(user);
        mUserRectanglesMap.get(user).remove(item);
    }

    public void onOwnerConfirmed(boolean changeOwner, int itemId, int userId) {

        if (!changeOwner) {
            return;
        }

        ItemRectangle item = null;
        User user = null;

        for (List<ItemRectangle> itemsPerUser : mUserRectanglesMap.values()) {
            item = getById(itemsPerUser, itemId);

            if (item != null) {
                break;
            }
        }

        user = getById(mUserRectanglesMap.keySet(), userId);

        if (item == null || user == null) {
            if (AppConfig.DEBUG) {
                throw new RuntimeException("Nulls!");
            }
        }

        addUser(item, user);

        mImageView.invalidate();
    }

    private <E extends Identified, T extends Collection<E>> E getById(T collection, int id) {

        for (E object : collection) {
            if (object.getId() == id) {
                return object;
            }
        }

        return null;
    }

    private User getCurrentUser() {

        return (User) mUsersSpinner.getSelectedItem();
    }

    private String recognizeRect(Rect rect) {

        mBaseApi.setRectangle(rect);

        return mBaseApi.getUTF8Text();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkExtras(KEY_IMAGE_PATH);

        setContentView(R.layout.activity_picker);

        mImagePath = getIntent().getStringExtra(KEY_IMAGE_PATH);
        mImageView = findView(R.id.fullscreen_image);

        final String uri = toFileUri(mImagePath);
        mImageView.setImage(ImageSource.uri(uri));
        setupImageView();

        mAnimals = getResources().getStringArray(R.array.animals);

        mUsersSpinner = findView(R.id.users_spinner);
        if (savedInstanceState != null) {
            mUsers = (ArrayList<User>) savedInstanceState.getSerializable(KEY_USERS);
        } else {
            addUser();
        }
        mAdapter = new UsersAdapter(mUsers, this);
        mUsersSpinner.setAdapter(mAdapter);

        TessHolderFragment tessHolderFragment = Utils.findFragment(getSupportFragmentManager(), TAG_HOLDER_FRAGMENT);
        if (tessHolderFragment != null) {
            mBaseApi = tessHolderFragment.mTessBaseAPI;
        }

        if (savedInstanceState == null) {
            recognize(MAIN_LANGUAGE);
        } else {
            mUserRectanglesMap = (Map<User, List<ItemRectangle>>) savedInstanceState.getSerializable(KEY_USERS_MAP);
            List<ItemRectangle> rectangles = new ArrayList<>();

            for (List<ItemRectangle> itemList : mUserRectanglesMap.values()) {
                rectangles.addAll(itemList);
            }

            mImageView.setItemRectangles(rectangles);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(KEY_USERS_MAP, (Serializable) mUserRectanglesMap);
        outState.putSerializable(KEY_USERS, mUsers);
    }



    private void recognize(final String lang) {

        new ProgressAsyncTask<Void, Void, Receipt>(getSupportFragmentManager()) {

            @Override
            public String getMessage() {
                return getString(R.string.recognizing);
            }

            @Override
            protected Receipt doInBackground(Void... params) {

                mBaseApi = new TessBaseAPI();
                mBaseApi.setDebug(AppConfig.DEBUG);

                mBaseApi.init(DATA_PATH, lang);

                Bitmap imageBitmap = BitmapFactory.decodeFile(mImagePath);
                mBaseApi.setImage(imageBitmap);

                try {
                   return recognizeUsingLibrary(mImagePath);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            protected void onPostExecute(Receipt receipt) {

                TessHolderFragment holderFragment = Utils.findFragment(getSupportFragmentManager(), TAG_HOLDER_FRAGMENT);
                if (holderFragment == null) {
                    holderFragment = new TessHolderFragment();
                    getSupportFragmentManager().beginTransaction().add(holderFragment, TAG_HOLDER_FRAGMENT).commit();
                }
                holderFragment.mTessBaseAPI = mBaseApi;

                parseResults(receipt);

                super.onPostExecute(receipt);

                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PickerActivity.this);
                if (!prefs.getBoolean(IntroDialog.KEY_INTRO_SHOWED, false)) {
                    new IntroDialog().show(getSupportFragmentManager(), null);
                }
            }


        }.execute();
    }

    public static class TessHolderFragment extends BasicFragment {

        public TessBaseAPI mTessBaseAPI;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setRetainInstance(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mBaseApi != null && !isChangingConfigurations()) {
            mBaseApi.end();
        }
    }

    private void processExit() {

        new ProgressAsyncTask<Void, Void, Void>(getSupportFragmentManager()) {

            @Override
            public String getMessage() {
                return getString(R.string.reading);
            }

            @Override
            protected Void doInBackground(Void... params) {

                for (Map.Entry<User, List<ItemRectangle>> entry : mUserRectanglesMap.entrySet()) {
                    final User user = entry.getKey();
                    final List<ItemRectangle> itemRectangles = entry.getValue();

                    user.mRecognizedItems.clear();

                    int i = 0;
                    String price;
                    String text = null;
                    for (ItemRectangle itemRect : itemRectangles) {
                        final Rect rect = toRect(itemRect.mRect);
                        if (i % 2 == 0) {
                            text = recognizeRect(rect);
                        } else {
                            price = recognizeRect(rect);
                            user.mRecognizedItems.add(new RecognizedItem(text, price));
                        }
                        i++;
                    }

                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                final ArrayList<User> actualUsers = new ArrayList<>(mUsers);
                removeEmptyUsers(actualUsers);

                ReportActivity.startActivity(PickerActivity.this, mImagePath, actualUsers);
            }

            private void removeEmptyUsers(List<User> users) {

                final Iterator<User> iterator = users.iterator();
                while (iterator.hasNext()) {
                    final User user = iterator.next();
                    if (user.mRecognizedItems.size() == 0) {
                        iterator.remove();
                    }
                }
            }
        }.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.activity_picker, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_undo) {
            performUndo();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void performUndo() {

        if (mHistory.size() == 0) {
            return;
        }

        final int location = mHistory.size() - 1;
        HistoryItem historyItem = mHistory.get(location);

        if (historyItem.mAction == HistoryItem.Action.ADDED) {
            mUserRectanglesMap.get(historyItem.mUser).remove(historyItem.mItemRectangle);

            historyItem.mItemRectangle.removeUser(historyItem.mUser);
            mImageView.invalidate();
        } else {
            historyItem.mItemRectangle.removeUser(historyItem.mUser);
            mImageView.invalidate();
        }

        mHistory.remove(location);
    }

    private Receipt recognizeUsingLibrary(String imagePath) throws Exception {

        try {
            int textHeight = 25;
            int textWidth = 1;

            ReceiptParser receiptParser = ReceiptParserFactory.getInstance(new File(imagePath), mTextRecognizer, textHeight, textWidth, true);
            return receiptParser.getReceipt();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private void parseResults(Receipt receipt) {

        for (TextRectangle priceRectangle : receipt.getPriceRectangles()) {

            RectF rect = toRectF(priceRectangle.getRectangle());
            mImageView.addRect(rect, PRICE);
        }

        for (TextRectangle textRectangle : receipt.getTextRectangles()) {

            RectF rect = toRectF(textRectangle.getRectangle());
            mImageView.addRect(rect, TEXT);
        }

    }

    private void setupImageView() {

        final GestureDetector gestureDetector = new GestureDetector(this, mGestureListener);

        mImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return gestureDetector.onTouchEvent(motionEvent);
            }
        });

    }

    public void onAcceptClicked(View view) {

        processExit();
    }

    public void onAddUserClicked(View view) {

        addUser();
    }

    private int getNewColor() throws OutOfListException {

        for (int presetColor : MATERIAL_PALETTE) {

            boolean colorPicked = false;
            for (int i = 0; i < mUsers.size() && !colorPicked; i++) {
                colorPicked = mUsers.get(i).mColor == presetColor;
            }

            if (!colorPicked) {
                return presetColor;
            }
        }

        throw new OutOfListException("Not enough colors.");
    }

    private String getNewName() throws OutOfListException {

        for (String animalName : mAnimals) {

            boolean colorPicked = false;
            for (int i = 0; i < mUsers.size() && !colorPicked; i++) {
                colorPicked = mUsers.get(i).mName.equals(animalName);
            }

            if (!colorPicked) {
                return animalName;
            }
        }

        throw new OutOfListException("Not enough animals.");
    }

    private void addUser() {

        try {
            String name = getNewName();
            final int color = getNewColor();
            mUsers.add(new User(name, color));

            mUsersSpinner.setSelection(mUsers.size() - 1);
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        } catch (OutOfListException e) {
            Toast.makeText(this, R.string.limit_reached, Toast.LENGTH_SHORT).show();
        }
    }

    public static class OwnerConfirmationDialog extends BasicDialogFragment {

        private static final String EXTRA_ITEM_ID = "itemId";
        private static final String EXTRA_USER_ID = "userId";

        public static OwnerConfirmationDialog newInstance(int itemId, int userId) {
            
            OwnerConfirmationDialog dialog = new OwnerConfirmationDialog();
            
            Bundle bundle = new Bundle();
            bundle.putInt(EXTRA_ITEM_ID, itemId);
            bundle.putInt(EXTRA_USER_ID, userId);

            dialog.setArguments(bundle);

            return dialog;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            checkExtras(EXTRA_ITEM_ID, EXTRA_USER_ID);
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final PickerActivity activity = (PickerActivity) getActivity();

            final int itemId = getArguments().getInt(EXTRA_ITEM_ID);
            final int userId = getArguments().getInt(EXTRA_USER_ID);

            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.owner_dialog_msg)
                    .setPositiveButton(R.string.owner_dialog_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            activity.onOwnerConfirmed(true, itemId, userId);
                        }
                    })
                    .setNegativeButton(R.string.owner_dialog_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            activity.onOwnerConfirmed(false, itemId, userId);
                        }
                    })
                    .create();
        }
    }

    public static class OutOfListException extends Exception {

        public OutOfListException(String detailMessage) {
            super(detailMessage);
        }
    }
}
