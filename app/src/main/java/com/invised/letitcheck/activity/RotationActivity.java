package com.invised.letitcheck.activity;

import android.content.Context;
import android.content.Intent;
import android.media.ExifInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.invised.letitcheck.R;
import com.invised.letitcheck.common.ProgressAsyncTask;
import com.invised.letitcheck.util.Utils;

import java.io.File;

import java.io.IOException;

public class RotationActivity extends BasicActivity {

    private static final String TAG = RotationActivity.class.getSimpleName();

    private static final String EXTRA_IMAGE_PATH = "imagePath";
    public static final String KEY_DEGREES = "degrees";

    private int mCurrentRotationDegrees = 0;
    private ImageView mImageView;
    private String mImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkExtras(EXTRA_IMAGE_PATH);

        setContentView(R.layout.activity_rotation);

        mImagePath = getIntent().getStringExtra(EXTRA_IMAGE_PATH);

        try {
            mCurrentRotationDegrees = getOrientationFromExif(mImagePath);
        } catch (IOException e) {
            Log.e(TAG, "Unable to get image exif orientation", e);
            e.printStackTrace();
            // Do nothing
        }

        mImageView = findView(R.id.image);

        if (savedInstanceState != null) {
            mCurrentRotationDegrees = savedInstanceState.getInt(KEY_DEGREES);
        }

        Glide.with(this)
                .load(mImagePath)
                .fitCenter()
                .into(mImageView);
    }

    private static int getOrientationFromExif(String imagePath) throws IOException {

        int orientation = 0;
        ExifInterface exif = new ExifInterface(imagePath);
        int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);

        switch (exifOrientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                orientation = 270;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                orientation = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                orientation = 90;
                break;
            case ExifInterface.ORIENTATION_NORMAL:
                orientation = 0;
                break;
            default:
                break;
        }


        return orientation;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.activity_rotation, menu);

        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(KEY_DEGREES, mCurrentRotationDegrees);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final int ROTATION_VALUE = 90;

        int sign;
        switch (item.getItemId()) {

            case R.id.rotate_left:
                sign = -1;
                break;
            case R.id.rotate_right:
                sign = 1;
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        int rotateBy = ROTATION_VALUE * sign;
        mCurrentRotationDegrees += rotateBy;

        mImageView.animate().rotation(mCurrentRotationDegrees);

        return true;
    }

    public static void startActivity(Context context, String imagePath) {

        Intent intent = new Intent(context, RotationActivity.class);
        intent.putExtra(EXTRA_IMAGE_PATH, imagePath);

        context.startActivity(intent);
    }

    public void onAcceptClicked(View view) {

        new ProgressAsyncTask<Void, Void, String>(getSupportFragmentManager()) {

            @Override
            protected String doInBackground(Void... params) {

                final String DIR = "temp";
                File picsDir = getDir(DIR, Context.MODE_PRIVATE);

                String fileName = picsDir + File.separator + "processed.png";

                int degrees = mCurrentRotationDegrees % 360;
                Utils.writeBitmap(fileName, Utils.createRotatedBitmap(degrees, mImagePath));

                return fileName;
            }

            @Override
            public String getMessage() {
                return getString(R.string.preparing);
            }

            @Override
            protected void onPostExecute(String imageFilename) {
                super.onPostExecute(imageFilename);

                PickerActivity.startActivity(RotationActivity.this, imageFilename);
                finish();
            }
        }.execute();

    }
}
