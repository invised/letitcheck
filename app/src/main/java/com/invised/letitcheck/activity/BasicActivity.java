package com.invised.letitcheck.activity;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.invised.letitcheck.util.Utils;

/**
 * Created by Invised on 09.09.2015.
 */
public class BasicActivity extends AppCompatActivity {

    protected Toolbar mToolbar;

    protected <T extends View> T findView(@IdRes int id) {
        return (T) findViewById(id);
    }

    protected <T extends View> T findView(Class<T> clazz, @IdRes int id) {
        return (T) findViewById(id);
    }


    public <T extends Fragment> T findFragment(int id) {
        return Utils.findFragment(getSupportFragmentManager(), id);
    }

    public <T extends android.app.Fragment> T findFragmentNative(int id) {
        return Utils.findFragment(getFragmentManager(), id);
    }

    protected void checkExtras(String... keys) {

        Utils.checkExtras(getIntent(), keys);
    }

    protected void checkIntegerExtras(String... keys) {

        Utils.checkIntegerExtras(getIntent(), keys);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        onSetBasicOrientation();
    }

    protected void onSetBasicOrientation() {
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            setHomeAsUpEnabled(true);
        }
    }

    protected void onHomePressed(MenuItem item) {

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onHomePressed(item);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void setHomeAsUpEnabled(boolean enabled) {
        final ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(enabled);
        }
    }

}
