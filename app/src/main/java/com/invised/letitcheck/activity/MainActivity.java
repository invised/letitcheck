package com.invised.letitcheck.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.invised.letitcheck.util.AppConfig;
import com.invised.letitcheck.R;
import com.invised.letitcheck.common.ProgressAsyncTask;
import com.invised.letitcheck.util.Utils;
import com.invised.letitcheck.fragment.dialogs.ImageSourceChooserDialog.PhotoSource;
import com.invised.letitcheck.fragment.ImageSourceHandlerFragment;

import java.io.File;
import java.io.IOException;

public class MainActivity extends BasicActivity
        implements ImageSourceHandlerFragment.OnPhotoReceivedListener  {

    private ImageSourceHandlerFragment mSourceHandlerFragment;

    public static final boolean FAST_START = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mSourceHandlerFragment = ImageSourceHandlerFragment.commit(getSupportFragmentManager(), null);

        ensureAssetsCopied();
    }

    private void onFilesReady() {

        if (FAST_START && AppConfig.DEBUG) {
            PickerActivity.startActivity(this, "/storage/emulated/0/Download/Checks/IMG_20151122_183836.jpg");
            finish();
        }
    }

    private void ensureAssetsCopied() {

        new ProgressAsyncTask<Void, Void, Void>(getSupportFragmentManager()) {

            public static final String TESS_TRAINED_EXT = ".traineddata";

            public static final String DIR_TESSDATA = "tessdata";
            private File TESS_FOLDER = new File("sdcard" + File.separator + DIR_TESSDATA);


            @Override
            public String getMessage() {
                return getString(R.string.files_preparing);
            }

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    if (TESS_FOLDER.exists()) {
                        final File[] files = TESS_FOLDER.listFiles();
                        final String[] assetsList = getAssets().list(DIR_TESSDATA);

                        int filesCount = 0;
                        for (File tessFile : files) {
                            if (tessFile.getName().endsWith(TESS_TRAINED_EXT)) {
                                filesCount++;
                            }
                        }

                        if (assetsList.length != filesCount) {
                            copyTrainedDataFiles();
                        }

                    } else {
                        copyTrainedDataFiles();
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                return null;
            }

            private void copyTrainedDataFiles() throws IOException {

                TESS_FOLDER.mkdir();
                Utils.copyAssets(DIR_TESSDATA, TESS_FOLDER, getAssets());
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                onFilesReady();

                super.onPostExecute(aVoid);
            }

        }.execute();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        setHomeAsUpEnabled(false);
    }

    @Override
    public void onPhotoReceived(String filePath, PhotoSource source) {

        if (source == PhotoSource.CAMERA) {
            Utils.scanMediaFiles(this, filePath);
        }

        RotationActivity.startActivity(this, filePath);
    }

    public void onPhotoFromGalleryClicked(View view) {

        mSourceHandlerFragment.dispatchGalleryIntent();
    }

    public void onPhotoFromCameraClicked(View view) {

        try {

            final File fileToSave = Utils.generateJpegFileName(Utils.getPublicPhotosDir());
            mSourceHandlerFragment.setFileToSave(fileToSave);

            mSourceHandlerFragment.dispatchTakePictureIntent();
        } catch (IOException ex) {
            Toast.makeText(this, R.string.error_temp_file, Toast.LENGTH_SHORT).show();
        }
    }

}
