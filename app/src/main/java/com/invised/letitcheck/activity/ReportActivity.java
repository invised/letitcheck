package com.invised.letitcheck.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.invised.letitcheck.R;
import com.invised.letitcheck.adapter.ReportAdapter;
import com.invised.letitcheck.recognizer.User;

import java.util.ArrayList;
import java.util.List;

public class ReportActivity extends BasicActivity {

    private static final String KEY_IMAGE_PATH = "image_path";
    private static final String KEY_USERS = "users";

    private List<User> mUsers;
    private ReportAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkExtras(KEY_USERS, KEY_IMAGE_PATH);

        setContentView(R.layout.activity_report);

        mUsers = (List<User>) getIntent().getSerializableExtra(KEY_USERS);

        RecyclerView list = findView(R.id.report_list);
        list.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new ReportAdapter(mUsers);
        list.setAdapter(mAdapter);

        Toolbar toolbar = findView(R.id.toolbar);
        setSupportActionBar(toolbar);

        String imagePath = getIntent().getStringExtra(KEY_IMAGE_PATH);
        ImageView imageView = findView(R.id.parallax_image);

        Glide.with(this)
                .load(imagePath)
                .centerCrop()
                .into(imageView);
    }

    public static void startActivity(Context context, String imagePath, ArrayList<User> users) {

        final Intent intent = new Intent(context, ReportActivity.class);
        intent.putExtra(ReportActivity.KEY_IMAGE_PATH, imagePath);
        intent.putExtra(ReportActivity.KEY_USERS, users);

        context.startActivity(intent);
    }



}
